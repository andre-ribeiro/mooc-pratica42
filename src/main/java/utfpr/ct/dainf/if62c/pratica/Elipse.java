/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author andre
 */
public class Elipse implements FiguraComEixos {
    protected double r,s;
    
    public Elipse(double _r, double _s) {
        this.r = _r;
        this.s = _s;
    }
    
    public double getArea() {
        return Math.PI*this.r*this.s;
    }
    
    public double getPerimetro() {
        return Math.PI*(3*(r+s) - Math.sqrt((3*r + s)*(r + 3*s)));
    }

    @Override
    public double getEixoMenor() {
        return r < s? r : s;
    }

    @Override
    public double getEixoMaior() {
        return r > s? r : s;
    }

    @Override
    public String getNome() {
        return "Elipse";
    }
    
    
}
