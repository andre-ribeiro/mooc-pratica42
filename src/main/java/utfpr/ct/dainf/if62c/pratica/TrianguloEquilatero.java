/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author andre
 */
public class TrianguloEquilatero implements FiguraComLados {

    protected double size;
    
    public TrianguloEquilatero(double _size) {
        this.size = _size;
    }
    
    @Override
    public double getLadoMenor() {
        return size;
    }

    @Override
    public double getLadoMaior() {
        return size;
    }

    @Override
    public String getNome() {
        return "TrianguloEquilatero";
    }

    @Override
    public double getPerimetro() {
        return 3*size;
    }

    @Override
    public double getArea() {
        return size*Math.sqrt(3)/4;
    }
    
}
